package utfpr.ct.dainf.if62c.projeto;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    protected double x, y, z;

    public Ponto()
    {
        this.x=0;
        this.y=0;
        this.z=0;
    }
    
    public Ponto(double x, double y, double z)
    {
        this.x=x;
        this.y=y;
        this.z=z;
    }
    public double getPontoX()
    {
        return this.x;
    }
    public double getPontoY()
    {
        return this.y;
    }
    public double getPontoZ()
    {
        return this.z;
    }
    public void setPontoX(double x)
    {
        this.x=x;
    }
    public void setPontoY(double y)
    {
        this.y=y;
    }
    public void setPontoZ(double z)
    {
        this.z=z;
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    @Override
    public String toString() {
        String nome;
        String nomeClasse = getNome();
        nome=nomeClasse+"("+x+y+z+")";
        return nome;
    }
    
    public boolean equals(Ponto p1, Ponto p2) {
             
        if((p1.x==p2.x)&&(p1.y==p2.y)&&(p1.z==p2.z))
           return true;
        else return false;
    }
    
    public double dist(Ponto p1, Ponto p2)
    {
        double dist=0;
        dist=Math.sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y)+(p2.z-p1.z)*(p2.z-p1.z));
        return dist;
    }
    
}
