/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Simone
 */
public class PoligonalFechada extends Poligonal{
    
    private Ponto2D[] vertices= new Ponto2D[100];
     
    public PoligonalFechada(Ponto2D vvertices[])
    {
        int tam=0;
        vertices=vvertices;
        tam=vertices.length;
        if(tam<2)
        {
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices!");
        }
    }
    
    @Override
    public double getComprimento()
    {
        double soma=0;
        int tam;
        tam = vertices.length;
        for(int i=0; i<(tam-2); i++)
        {
            soma+=dist(vertices[i], vertices[tam-1]);
        }
        soma+=dist(vertices[0],vertices[tam-1]);
        return soma;
    }
}
