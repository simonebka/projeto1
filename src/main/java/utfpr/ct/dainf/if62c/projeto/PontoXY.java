/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Simone
 */
public class PontoXY extends Ponto2D{
    protected PontoXY()
    {
        super();
    }
    
    protected PontoXY(double x, double y)
    {
        this.x=x;
        this.y=y;
        this.z=0;
    }
@Override
    public String toString() {
        String nome;
        String nomeClasse = getNome();
        nome=nomeClasse+"("+x+y+")";
        return nome;
    }
}
