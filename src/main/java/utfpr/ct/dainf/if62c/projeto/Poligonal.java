/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Simone
 * @param <T>
 */
public class Poligonal<T> {
    private T info;
    private Ponto2D[] vertices= new Ponto2D[100];
    
    public Poligonal(){}
    
    public Poligonal(Ponto2D vvertices[])
    {
        int tam=0;
        vertices=vvertices;
        tam=vertices.length;
        if(tam<2)
        {
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices!");
        }
    }
    
    public int getN()
    {
        int tam;
        tam=vertices.length;
        return tam;
    }
    
    public T getInfo(T info)
    {
        return info;
    }
    
    public Ponto2D get(int pos)
    {
        int tam;
        tam=getN();
        if ((pos<0)|| pos>(tam-1))
           return null;
        else  return vertices[pos];
    }
    
    public void set( int pos, Ponto2D pon)
    {
        int tam;
        tam=getN();
        if ((pos>0)&& pos<(tam-1))
           vertices[pos]=pon;
    }
    
    public double getComprimento()
    {
        double soma=0;
        for(int i=0; i<(vertices.length-2); i++)
        {
            soma+=dist(vertices[i], vertices[i+1]);
        }
        return soma;
    }
    
    public double dist(Ponto2D p1, Ponto2D p2)
    {
        double dist=0;
        dist=Math.sqrt((p2.x-p1.x)*(p2.x-p1.x)+(p2.y-p1.y)*(p2.y-p1.y)+(p2.z-p1.z)*(p2.z-p1.z));
        return dist;
    }
    
    public void setInfo(T info)
    {
        this.info=info;
    }
    
}
