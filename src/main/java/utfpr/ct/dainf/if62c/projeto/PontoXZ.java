/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Simone
 */
public class PontoXZ extends Ponto2D{
    protected PontoXZ()
    {
        super();
    }
    
    protected PontoXZ(double x, double z)
    {
        this.x=x;
        this.y=0;
        this.z=z;
    }
    @Override
    public String toString() {
        String nome;
        String nomeClasse = getNome();
        nome=nomeClasse+"("+x+z+")";
        return nome;
    }
}
