/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Simone
 */
public class PontoYZ extends Ponto2D {
    protected PontoYZ()
    {
        super();
    }
    
    protected PontoYZ(double y, double z)
    {
        this.x=0;
        this.y=y;
        this.z=z;
    }
@Override
    public String toString() {
        String nome;
        String nomeClasse = getNome();
        nome=nomeClasse+"("+y+z+")";
        return nome;
    }
}
