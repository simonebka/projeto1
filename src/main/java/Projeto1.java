
import utfpr.ct.dainf.if62c.projeto.PoligonalFechada;
import utfpr.ct.dainf.if62c.projeto.Ponto2D;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    public static void main(String[] args) {

        double comp=0;
        Ponto2D[] vert = new Ponto2D[100];
        vert[0].setPontoX(-3);
        vert[0].setPontoZ(2);
        vert[1].setPontoX(-3);
        vert[1].setPontoZ(6);
        vert[2].setPontoX(0);
        vert[2].setPontoZ(2);
         PoligonalFechada pfxz = new PoligonalFechada(vert);
        comp=pfxz.getComprimento();
        System.out.println("Comprimento da poligonal"+comp);
    }
    
}
